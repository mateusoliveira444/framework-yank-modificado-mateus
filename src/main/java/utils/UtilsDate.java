/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Mateus Oliveira
 */
public class UtilsDate {

    public static String getYear() {
        DateFormat dateFormatYear = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String dateYear = dateFormatYear.format(date);
        return dateYear;
    }

    public static String getMonth() {
        DateFormat dateFormatMonth = new SimpleDateFormat("MM");
        Date date = new Date();
        String dateMonth = dateFormatMonth.format(date);
        return dateMonth;
    }

    public static String getDay() {
        DateFormat dateFormatDay = new SimpleDateFormat("dd");
        Date date = new Date();
        String dateDay = dateFormatDay.format(date);
        return dateDay;
    }

    public static String getYesterday() {
        DateFormat dateFormatDay = new SimpleDateFormat("dd");
        String dateDay = dateFormatDay.format(yesterday());
        return dateDay;
    }

    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static Date lastWeek() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -13);
        return cal.getTime();
    }

    public String getLasttUilDay() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayOfWeek;
        do {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        } while (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY);

        return dateFormat.format(cal.getTime());

    }

    public static String formatDateEnglish(String dateToFormat) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date data = formato.parse(dateToFormat);
        formato.applyPattern("yyyy/MM/dd HH:mm:ss");
        return formato.format(data);
    }

    public static String formatStringDatePt(String dateToFormat) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date data = formato.parse(dateToFormat);
        formato.applyPattern("dd/MM/yyyy HH:mm:ss");
        return formato.format(data);
    }

    public static Date formatStringDateToPt(String dateToFormat) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date data = formato.parse(dateToFormat);
        formato.applyPattern("dd/MM/yyyy HH:mm");
        String format = formato.format(data);
        return formato.parse(format);

    }

    public static Date formatDatePt(Date dateToFormat) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String data = formato.format(dateToFormat);
        return formato.parse(data);
    }

    public static String formatDatePtToString(Date dateToFormat) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String data = formato.format(dateToFormat);
        return data;
    }

    public static XMLGregorianCalendar formatDatePtGregorianCalendar(String dateToFormat) throws ParseException, DatatypeConfigurationException {
        dateToFormat = dateToFormat.replaceAll("-", "/");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date data = formato.parse(dateToFormat);
        formato.applyPattern("dd/MM/yyyy HH:mm:ss");
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(data);

        return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
    }

    public static String dataHoraAtual() {
        Calendar dataAtual;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dataAtual = new GregorianCalendar();
        Date now = dataAtual.getTime();
        return sdf.format(now);
    }

    //SUBTRAI SEGUNDA DATA, DA PRIMEIRA.
    public static int differenceBetweenDates(String t1, String t2) throws ParseException {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String inputString1 = t1;
        String inputString2 = t2;

        Date date1 = myFormat.parse(inputString1);
        Date date2 = myFormat.parse(inputString2);
        int diff = (int) (date2.getTime() - date1.getTime());
        int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        return (int) days;
    }

    public static String convertXMLGregorianCalendar(XMLGregorianCalendar data) {
        Calendar calendar = data.toGregorianCalendar();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        formatter.applyPattern("dd/MM/yyyy HH:mm");
        String dateString = formatter.format(calendar.getTime());
        return dateString;
    }

    public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        int workDays = 0;
        int hours = 0;
        //Return 0 if start and end are the same

        while (startCal.getTimeInMillis() < endCal.getTimeInMillis()) {

            //excluding start date
            startCal.add(Calendar.HOUR_OF_DAY, 1);
            if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++hours;
                if (hours % 24 == 0) {
                    ++workDays;
                }
            }
        }
        return workDays;
    }
}
