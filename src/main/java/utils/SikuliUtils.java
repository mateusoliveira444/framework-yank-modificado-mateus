package utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import org.sikuli.script.FindFailed;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


/**
 *
 * @author SRADMIN
 */
public class SikuliUtils {

    private Screen screen;
    private int averageTime = 1000;
    private String pathDefaultDirectory;

    public SikuliUtils(String pathDefault) {
        screen = new Screen();
        pathDefaultDirectory = pathDefault;
    }

    public void maximize(String pathBarra) throws FindFailed, InterruptedException {
        screen.doubleClick(pathDefaultDirectory + pathBarra);
        Thread.sleep(1000);
    }

    /**
     * O método Copy Selected Text Simplesmente aciona as teclas CTRL + C, colocando o texto selecionado 
     * para a àrea de transferencia compartilhada com o Sikuli.
     * @return
     * @throws IOException
     * @throws UnsupportedFlavorException
     * @throws InterruptedException 
     */
    public String copySelectedText() throws IOException, UnsupportedFlavorException, InterruptedException {
        String retorno = "";
        StringSelection stringSelection = new StringSelection("");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        while (true) {
            try {
                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(stringSelection, null);
                break;
            } catch (Exception e) {
                continue;
            }
        }

        Thread.sleep(2000);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
        screen.type("c", KeyModifier.CTRL);
//        screen.keyDown(Key.CTRL);
//        Thread.sleep(500);
//        screen.type("c");
//        Thread.sleep(500);
//        screen.keyUp(Key.CTRL);
        Thread.sleep(2000);
        retorno = (String) clipboard.getData(DataFlavor.stringFlavor);
        return retorno;
    }

    /**
     * O método Get First Match recebe uma lista de Patterns que deve encontrar;
     * Um delay(em segundos) para esperar cada Pattern; e um int tries
     * (tentativas) para encotrar a lista. O caso encontre algum Pattern, o
     * retorna imediatamente. Caso não encontre nenhum até o fim do tempo
     * retorna um Pattern = null.
     *
     * @param list
     * @param delay
     * @param tries
     * @return
     * @throws FindFailed
     */
    public Pattern getFirstPatternMatch(List<Pattern> list, int delay, int tries) throws FindFailed {
        for (int count = 0; count < tries; count++) {
            for (Pattern select : list) {
                try {
                    wait(select, delay);
                    return select;
                } catch (Exception e) {
                    //shut Up and get me the next one
                }
            }
            System.out.println(count + "ª tentantiva. Nenhuma das opções encontradas.");
        }
        System.out.println("Não foi possível encontrar nenhuma imagem.");
        Pattern empty = null;
        return empty;
    }

    /**
     * O método get First Img Match 
     * @param examples
     * @param delay
     * @param tries
     * @return
     * @throws FindFailed 
     */
    public String getFirstImgMatch(List<String> examples, int delay, int tries) throws FindFailed {
        for (int count = 0; count < tries; count++) {
            for (String select : examples) {
                try {
                    wait(pathDefaultDirectory + select, delay);
                    return select;
                } catch (Exception e) {
                    //shut Up and get me the next one
                }
            }
            System.out.println(count + "ª tentantiva. Nenhuma das opções encontradas.");
        }
        System.out.println("Não foi possível encontrar nenhuma imagem.");
        return "";
    }

    /**
     * O método Click Region recebe uma diretriz X e Y (int) que se refere ao ponto em pixels que
     * começa a região da imagem.
     * Recebe tambem o comprimento e altura (W e H, respectivamente). Com estes dados Cria uma imagem e a 
     * salva na pasta "Dinamicas" que será criada dentro da pasta de imagens instanciadas no Sikuli.
     * Após cria a imagem com o conteudo dinâmico, clica no centro dela.
     * @param x
     * @param y
     * @param w
     * @param h
     * @param titulo
     * @throws IOException
     * @throws FindFailed 
     */
    public void clickRegion(int x, int y, int w, int h, String titulo) throws IOException, FindFailed {

        File dir = new File(pathDefaultDirectory + "dinamicas");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File img = new File(pathDefaultDirectory + "dinamicas/" + titulo + ".png");

        ImageIO.write(screen.capture(x, y, w, h).getImage(), "png", img);

        for (int count = 0; count <= 2; count++) {
            try {
                screen.click(pathDefaultDirectory + "dinamicas/" + titulo + ".png");
                return;
            } catch (FindFailed e) {
                if (e.getMessage().equals("Text search currently switched off")) {
                    continue;
                } else {
                    e.printStackTrace();
                }
            }
        }
        throw new FindFailed("Falhou a busca dinamica");
    }

    /**
     * A função waitDiference recebe uma imagem e a quantidade de segundos que
     * deve esperar até que a área de imagem enviada sofra alguma alteração.
     *
     * @param path
     * @param times
     * @throws Exception
     */
    public void waitDiference(String path, int times) throws Exception {
        for (int count = times; count > 0; count--) {
            try {
                wait(pathDefaultDirectory + path, 1);
                System.out.println("aguardando alterações");
                Thread.sleep(1000);
            } catch (Exception err) {
                if (err.getMessage().equals("A busca falhou")) {
                    System.out.println("Houveram alterações");
                    return;
                }
            }
        }
        throw new Exception("Não houve alterações");
    }

    /**
     * O método Double Click recebe o caminho de uma imagem que deve ser
     * procurada. Quando o Sikuli Encontra, da um clique duplo no centro da
     * imagem. Caso não encontra, levanta "NotFoundException"
     *
     * @param path
     * @throws FindFailed
     */
    public void doubleClick(String path) throws FindFailed {
        screen.wait(pathDefaultDirectory + path);
        screen.doubleClick(pathDefaultDirectory + path);
    }

    /**
     * O método Click recebe o caminho de uma imagem que deve ser procurada.
     * Quando o Sikuli Encontra, da um clique no centro da imagem. Caso não
     * encontra, levanta "NotFoundException"
     *
     * @param path
     * @throws FindFailed
     */
    public void click(String path) throws FindFailed {
        System.out.println("clickando em.: " + path);
        screen.wait(pathDefaultDirectory + path);
        screen.click(pathDefaultDirectory + path);
    }

    /**
     * O método Click recebe um Pattern da imagem em que deve clicar. Quando o
     * Sikuli Encontra, da um clique no centro da imagem. Caso não encontra,
     * levanta "NotFoundException"
     *
     * @param select
     * @throws FindFailed
     */
    public void click(Pattern select) throws FindFailed {
        this.screen.click(select);
    }

    /**
     * O método Right Click recebe um Pattern da imagem em que deve clicar com o
     * Botão direito do mouse. Quando o Sikuli Encontra, da um clique com o
     * botão direito do mouse no centro da imagem. Caso não encontra, levanta
     * "NotFoundException"
     *
     * @param select
     * @throws FindFailed
     */
    public void rightClick(String path) throws FindFailed {
        System.out.println("click direito em.: " + path);
        screen.wait(pathDefaultDirectory + path);
        screen.rightClick(pathDefaultDirectory + path);
    }

    /**
     * O método Right Click recebe um Pattern da imagem em que deve clicar com o
     * Botão direito do mouse. Quando o Sikuli Encontra, da um clique com o
     * botão direito do mouse no centro da imagem. Caso não encontra, levanta
     * "NotFoundException"
     *
     * @param select
     * @throws FindFailed
     */
    public void rightClick(Pattern select) throws FindFailed {
        this.screen.rightClick(select);
    }

    /**
     * O método Send Keys in Loop recebe um objeto Key (que se refere a um botão
     * de teclado/mouse) e a quantidade que deve ser apertado em loop. Esta
     * função não recebe o Delay, portanto o intervalo entre uma acionamento da
     * Key e o proximo é minimo e varia de acordo com o Ambiente.
     *
     * @param path
     * @throws FindFailed
     */
    public void sendKeysInLoop(String key, int loop) {
        for (int count = 0; count < loop; count++) {
            this.screen.type(key);
        }
    }

    public void sendKeyChain(String[] chain, int delay) throws InterruptedException {
        for (int index = 0; index < chain.length; index++) {
            screen.type(chain[index]);
            Thread.sleep(delay);
        }
    }

    /**
     * O método Send Keys in Loop recebe um objeto Key (que se refere a um botão
     * de teclado/mouse) e a quantidade que deve ser apertado em loop. Recebe
     * tambem um int "delay" que se refere ao tempo (em Milissengundos) que
     * devemos esperar entre um acionamento da Key e o subsquente.
     *
     * @param key
     * @param loop
     * @param delay
     * @throws InterruptedException
     */
    public void sendKeysInLoop(String key, int loop, int delay) throws InterruptedException {
        for (int count = 0; count < loop; count++) {
            this.screen.type(key);
            Thread.sleep(delay);
        }
    }

    public void sendKeyModifierInLoop(String key, int keyModifier, int loop, int delay) throws InterruptedException {
        for (int count = 0; count < loop; count++) {
            this.screen.type(key, keyModifier);
            Thread.sleep(delay);
        }
    }

    /**
     * A função Wait recebe o caminho de uma imagem que deve procurar, e um int
     * (segundos) que o sikuli deve esperar pela imagem. A função termina quando
     * achar a imagem, ou quando o contador (timeout) chegar a 0. Caso chegar a
     * 0 e não encontrar a imagem, levanta "FindFailed Exception"
     *
     * @param path
     * @param timeout
     * @throws FindFailed
     * @throws InterruptedException
     * @throws Exception
     */
    public void waitSeconds(String path, int timeout) throws FindFailed, InterruptedException, Exception {
        System.out.println("buscando " + path);
        for (int count = timeout; count > 0; count--) {
            System.out.println("\ttentativa:" + String.valueOf(count));
            try {
                this.screen.wait(pathDefaultDirectory + path);
                return;
            } catch (FindFailed e) {
                Thread.sleep(1000);
            }
        }
        throw new FindFailed("Imagem '" + path + "' não localizada");
    }
    /**
     * A função Wait recebe o caminho de uma imagem que deve procurar, e um int
     * (milil
     * segundos) que o sikuli deve esperar pela imagem. A função termina quando
     * achar a imagem, ou quando o contador (timeout) chegar a 0. Caso chegar a
     * 0 e não encontrar a imagem, levanta "FindFailed Exception"
     *
     * @param path
     * @param timeout
     * @throws FindFailed
     * @throws InterruptedException
     * @throws Exception
     */
    public void wait(String path, int timeout) throws FindFailed, InterruptedException, Exception {
        System.out.println("buscando " + path);
        for (int count = timeout; count > 0; count--) {
            System.out.println("\ttentativa:" + String.valueOf(count));
            try {
                this.screen.wait(pathDefaultDirectory + path);
                return;
            } catch (FindFailed e) {
                Thread.sleep(1);
            }
        }
        throw new FindFailed("Imagem '" + path + "' não localizada");
    }

    /**
     * A função Wait recebe um Pattern de uma imagem que deve procurar, e um int
     * (segundos) que o sikuli deve esperar pela imagem. A função termina quando
     * achar a imagem, ou quando o contador (timeout) chegar a 0. Caso chegar a
     * 0 e não encontrar a imagem, levanta "FindFailed Exception"
     *
     * @param Pattern
     * @param timeout
     * @throws FindFailed
     * @throws InterruptedException
     * @throws Exception
     */
    public void wait(Pattern select, int timeout) throws FindFailed, InterruptedException, Exception {
//        System.out.println("buscando " + path);
        for (int count = timeout; count > 0; count--) {
            System.out.println("\ttentativa:" + String.valueOf(count));
            try {
                this.screen.wait(select);
                return;
            } catch (FindFailed e) {
                Thread.sleep(1000);
            }
        }
        throw new FindFailed("Imagem não localizada");
    }

    /**
     * O método Start Process abre o "Run" do Ruindows e Digita o comando
     * (enviado por parametro - String).
     *
     * @param command
     * @throws InterruptedException
     */
    public void startProcess(String command) throws InterruptedException {
        this.screen.type("r", KeyModifier.WIN);
        Thread.sleep(500);
        this.screen.paste(command);
        Thread.sleep(100);
        this.screen.type("\n");
    }

    /**
     * @return the screen
     */
    public Screen getScreen() {
        return screen;
    }

    /**
     * @param screen the screen to set
     */
    public void setScreen(Screen screen) {
        this.screen = screen;
    }
}
