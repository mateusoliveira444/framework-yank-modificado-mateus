/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Mateus Oliveira
 */
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;
import javax.mail.Folder;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtils {

    private Session sessionEmail;
    private String host;
    private String login;
    private String password;
    private int port;
    public String user = null;

    public EmailUtils() throws MessagingException {
        sessionEmail = createSessionEmail();
    }

    public Session createSessionEmail() throws MessagingException {

        //create properties field
        Properties props = new Properties();

        props.put("mail.smtp.auth", "true");
        props.put("mail.imap.ssl.trust", "*");
        props.setProperty("mail.smtp.host", "zimbra.jbmlaw.com.br");
        props.put("mail.smtp.port", 587);

        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.imap.auth.plain.disable", "true");
        props.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.imap.socketFactory.fallback", "false");
        props.put("mail.mime.charset", "ISO-8859-1");
        props.put("mail.smtp.ssl.trust", "*");

        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(login, password);
            }
        };
        Session emailJBMSession = Session.getDefaultInstance(props, auth);
//        emailJBMSession.setDebug(true);

        return emailJBMSession;
    }

    public void connectMailBox() throws IOException, ParseException {
        //TIPS
        // TRY PORT = 993 for imap
        try {
            //create the IMAP store object and connect with the pop server
            Store store = sessionEmail.getStore("imap");
            store.connect(host, port, login, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            readMailBox(messages);

            //close the store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

    public void readMailBox(Message[] messages) throws IOException, MessagingException, ParseException {
        // CONTADOR DE QUANTAS LINHAS EXISTEM NO BANCO, POIS EQUIVALE AO NUMERO DE EMAILS QUE JA FORAM ADICIONADOS
        // PODENDO ASSIM COMEÇAR A LER A CAIXA DE ENTRADA DE UM E-MAIL MAIS A FRENTE, NÃO TENDO QUE VOLTAR SEMPRE DO 0
//        int contaLinhasBd = daoEmail.contaLinhasBd();

        for (int i = 0, n = messages.length; i < n; i++) {
            Message message = messages[i];

            Multipart mp = null;
            while (true) {
                try {
                    mp = (Multipart) message.getContent();
                    break;
                } catch (IOException e) {
                } catch (MessagingException iOException) {
                }
            }
            String contentEmail = "";
            System.out.println(message.getReceivedDate());
            System.out.println("---------------------------------");
            System.out.println("Email Number " + (i + 1));
            System.out.println("Subject: " + message.getSubject());
            System.out.println("From: " + message.getFrom()[0]);
            for (int l = 0; l < mp.getCount(); l++) {
                BodyPart bodyPart = mp.getBodyPart(l);
                if (bodyPart.isMimeType("text/plain")) {
                    String s = (String) bodyPart.getContent();
//                        System.out.println("string = " + s);
                } else if (message.isMimeType("multipart/*")) {
//                        System.out.println("BODYPART : " + l);
                    MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                    contentEmail = getTextFromMimeMultipart(mimeMultipart);
                }
            }
            System.out.println("----------------------------------");
        }
    }

    public void sendEmail(String from, String toEmail[], String subject, String body) throws IOException {
        //COM ASSINATURA
//        String html = "<html><body>" + body.replaceAll("\n", "<br>") + " <br><br><br>  <img src='cid:my-image-id'></body></html>";
        //SEM ASSINATURA

        String html = "<html><body>" + body.replaceAll("\n", "<br>") + " <br><br><br> </body></html>";

        try {
            MimeMessage msg = new MimeMessage(sessionEmail);
            msg.addHeader("Content-type", "text/html");

            msg.setFrom(new InternetAddress(from));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            // CASO QUEIRA ENVIAR COM ASSINATURA, SALVAR IMAGEM DE ASSINATURA NA PASTA E ENVIAR O PATH
//            DataSource fds = new FileDataSource(pathAssinatura);

            for (int i = 0; i < toEmail.length; i++) {
                msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail[i]));
            }

            Multipart multipart = new MimeMultipart();

            // Create the message body part
            MimeBodyPart htmlPart = new MimeBodyPart();
            // Fill the message
            htmlPart.setContent(html, "text/html");
//
            MimeBodyPart imagePart = new MimeBodyPart();

            //DESCOMENTE LINHAS ABAIXO CASO FOR ENVIAR ASSINATURA
//            imagePart.attachFile("C:\\Users\\Mateus Oliveira\\Documents\\NetBeansProjects\\JBM - ADV - Procon\\assinatura.jpg");
//            imagePart.setDisposition(MimeBodyPart.INLINE);
//            imagePart.setDataHandler(new DataHandler(fds));
//            imagePart.setHeader("Content-ID", "my-image-id");
            // Create a multipart message for attachment
            // Set text message part
            multipart.addBodyPart(htmlPart);
            multipart.addBodyPart(imagePart);
            // Send the complete message parts
            msg.setContent(multipart);
            // Send message
            Transport.send(msg);
            System.out.println("Email Sent Successfully");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    //método para dar reply com anexo
    public void replyEmailWithAnexo(String login, String password, String from, String toEmail[], String subject, String body, String filename) throws IOException, InterruptedException, NoSuchProviderException, MessagingException {
        Session readEmail = sessionEmail;
        Store store = readEmail.getStore("imap");
        store.connect("zimbra.jbmlaw.com.br", 993, login, password);
        //create the folder object and open it
        Folder emailFolder = store.getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);

        // retrieve the messages from the folder in an array and print it
        Message[] messages = emailFolder.getMessages();
        System.out.println("messages.length---" + messages.length);
        Message msgToReply = null;
        for (int i = 300, n = messages.length; i < n; i++) {
            Message message = messages[i];
            if (message.getSubject().contains(subject)) {
                Address[] replyTo = message.getReplyTo();
                msgToReply = message;
                break;
            }
        }

//        Session createSessionSendEmailJBM = createSessionSendEmailJBM();
        if (msgToReply != null) {
            String html = "<html><body>" + body.replaceAll("\n", "<br>") + " <br><br><br>  <img src='cid:my-image-id'></body></html>";

            try {
                MimeMessage msg = new MimeMessage(sessionEmail);
                msg.addHeader("Content-type", "text/html");
                msg.setFrom(new InternetAddress(from));
                msg.setSubject(subject);
                msg.setSentDate(new Date());
                DataSource fdsAssinatura = new FileDataSource("assinatura.jpg");
                DataSource fdsAttach = new FileDataSource(filename);

                msg = (MimeMessage) msgToReply.reply(false);

                for (int i = 0; i < toEmail.length; i++) {
                    msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail[i]));
                }

                Multipart multipart = new MimeMultipart();
                // Create the message body part
                MimeBodyPart htmlPart = new MimeBodyPart();
                MimeBodyPart imagePart = new MimeBodyPart();
                MimeBodyPart attachFilePart = new MimeBodyPart();

                //INSERE ASSINATURA
                imagePart.attachFile("assinatura.jpg");
                imagePart.setDisposition(MimeBodyPart.INLINE);
                imagePart.setDataHandler(new DataHandler(fdsAssinatura));
                imagePart.setHeader("Content-ID", "my-image-id");

//            attachFilePart.setDataHandler(new DataHandler(fds));
//            attachFilePart.setFileName("ATA.PDF");
                // Fill the message
                htmlPart.setContent(html, "text/html;charset=UTF-8");
                //Anexo
                attachFilePart.setDataHandler(new DataHandler(fdsAttach));
                attachFilePart.setFileName(filename);

                // Create a multipart message for attachment
                // Set text message part
                multipart.addBodyPart(htmlPart);
                multipart.addBodyPart(imagePart);
                multipart.addBodyPart(attachFilePart);
                // Send the complete message parts
                msg.setContent(multipart);
                readEmail = null;
                // Send message
                int cont = 0;
                while (cont <= 10) {
                    try {
                        Transport.send(msg, msg.getAllRecipients());
                        break;
                    } catch (MessagingException e) {
                        cont++;
                        Thread.sleep(500);
                        e.printStackTrace();
                    }
                }

                System.out.println("Email Sent Successfully with attachment!!");
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            emailFolder.close(false);
            store.close();
        }

    }

    public void sendEmailAnexo(Session session, String toEmail[], String subject, String body, String filename) throws InterruptedException, IOException {
        String html = "<html><body>" + body.replaceAll("\n", "<br>") + "  <br><img src='cid:my-image-id'></body></html>";

        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/html; charset=UTF-8");
            msg.addHeader("format", "flowed");

            msg.setFrom(new InternetAddress("procon.bradesco@jbmlaw.com.br"));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            DataSource fdsImage = new FileDataSource("C:\\Users\\Mateus Oliveira\\Documents\\NetBeansProjects\\JBM - ADV - Procon\\assinatura.jpg");
            DataSource fdsAttach = new FileDataSource("C:\\Users\\Mateus Oliveira\\Documents\\NetBeansProjects\\JBM - ADV - Procon\\" + filename);

            for (int i = 0; i < toEmail.length; i++) {
                msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail[i]));
            }
            Multipart multipart = new MimeMultipart();

            // Create the message body part
            MimeBodyPart htmlPart = new MimeBodyPart();
            MimeBodyPart imagePart = new MimeBodyPart();
            MimeBodyPart attachFilePart = new MimeBodyPart();

            //INSERE ASSINATURA
            imagePart.attachFile("C:\\Users\\Mateus Oliveira\\Documents\\NetBeansProjects\\JBM - ADV - Procon\\assinatura.jpg");
            imagePart.setDisposition(MimeBodyPart.INLINE);
            imagePart.setDataHandler(new DataHandler(fdsImage));
            imagePart.setHeader("Content-ID", "my-image-id");

//            attachFilePart.setDataHandler(new DataHandler(fds));
//            attachFilePart.setFileName("ATA.PDF");
            // Fill the message
            htmlPart.setContent(html, "text/html;charset=UTF-8");
            //Anexo
            attachFilePart.setDataHandler(new DataHandler(fdsAttach));
            attachFilePart.setFileName(filename);

            // Create a multipart message for attachment
            // Set text message part
            multipart.addBodyPart(htmlPart);
            multipart.addBodyPart(imagePart);
            multipart.addBodyPart(attachFilePart);
            // Send the complete message parts
            msg.setContent(multipart);

            // Send message
            int cont = 0;
            while (cont <= 10) {
                try {
                    Transport.send(msg);
                    cont++;
                    break;
                } catch (MessagingException e) {
                    Thread.sleep(500);
                    e.printStackTrace();
                }
            }
            System.out.println("Email Sent Successfully with attachment!!");
        } catch (MessagingException e) {
            e.printStackTrace();

        }

    }

}
