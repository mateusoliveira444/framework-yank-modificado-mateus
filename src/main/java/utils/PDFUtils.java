package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mateus Oliveira
 */
public class PDFUtils {

    public static void pdfStripper(String path) throws IOException, InterruptedException {
        PDDocument document = PDDocument.load(new File("C:\\Users\\Mateus Oliveira\\Downloads\\" + path));
        document.getClass();
        if (!document.isEncrypted()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);

            PDFTextStripper tStripper = new PDFTextStripper();

            String pdfFileInText = tStripper.getText(document);
            //System.out.println("Text:" + st);

            // split by whitespace
            String lines[] = pdfFileInText.split("\\n");
            String[] allStrings = new String[lines.length];

            for (int i = 0; i < allStrings.length; i++) {
                allStrings[i] = "";
            }

            int cont = 0;
            boolean pulalinha = false;
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i];

                //SEPARA POR PARAGRAFO
                if (line.contains("\r") || line.contains("\n")) {
                    if (pulalinha) {
                        cont++;
                    }
                    pulalinha = false;
                    continue;
                }

                pulalinha = true;
                allStrings[cont] += line;

            }

        }

    }

}
