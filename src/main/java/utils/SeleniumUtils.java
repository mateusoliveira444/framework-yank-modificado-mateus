/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
//import tools.TextFormater;

/**
 *
 * @author SRADMIN
 */
public class SeleniumUtils {

    private WebDriver driver;
    private JavascriptExecutor jse;
    private boolean found = false;
    private boolean waitLoad = false;
    private Actions action;

    /**
     * Instancia um novo Chrome Driver e seta a Variável Driver Utilize o Método
     * getDriver() para manipular.
     */
    public void buildChromeDriver() {
        if (SystemUtils.getOperationalSystem().toUpperCase().contains("WINDOWS")) {
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        } else {
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        }
        this.setDriver(new ChromeDriver());
        this.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.MILLISECONDS);
        this.setJse((JavascriptExecutor) getDriver());

    }

    /**
     * Instancia um novo IE Driver e seta a Variável Driver Utilize o Método
     * getDriver() para manipular.
     */
    public void buildIEDriver() {
        System.setProperty("webdriver.ie.driver", "drivers/IEDriverServer.exe");
        //DESABILITA MODO PROTEGIDO PARA TODAS AS ZONAS
        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
        cap.setCapability("ignoreZoomSetting", true);
        cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        this.setDriver(new InternetExplorerDriver(cap));
        this.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.MILLISECONDS);
        this.setJse((JavascriptExecutor) getDriver());
        this.setAction((Actions) getDriver());

    }

    /**
     * Instancia um novo Firefox Driver e seta a Variável Driver Utilize o
     * Método getDriver() para manipular.
     */
    public void buildFireFoxDriver() {
        if (SystemUtils.getOperationalSystem().toUpperCase().contains("WINDOWS")) {
            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
        } else {
            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
        }
        this.setDriver(new FirefoxDriver());
        this.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.MILLISECONDS);
        this.setJse((JavascriptExecutor) getDriver());
        this.setAction((Actions) getDriver());

    }

    public WebElement huntElementInMyWindow(By by) {
        System.out.println("Começando a caça do Elemento :" + by);
        setFound(false);
        elementHuntRoadMap(by);
        return getDriver().findElement(by);
    }

    public String retiraTodosAcentos(String string) {

        String acentos = string;

        acentos = Normalizer.normalize(acentos, Normalizer.Form.NFD);
        String semAcentos = acentos.replaceAll("[^\\p{ASCII}]", "");

        return semAcentos;
    }

    public String removeAcentos(String s) {
        s = s.replaceAll("ê", "e").replaceAll("í", "i").replaceAll("á", "a").replaceAll("ó", "o").replaceAll("ú", "u").replaceAll("Á", "A")
                .replaceAll("É", "E").replaceAll("Í", "I").replaceAll("Ó", "O").replaceAll("Ú", "U").replaceAll("Ê", "E").replaceAll("À", "A")
                .replaceAll("È", "E").replaceAll("é", "e");
        return s;
    }

    public String singleRegexMatcher(String texto, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(texto);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "Pattern not found";
    }

    public List<String> everyRegexMatcher(String texto, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(texto);
        List<String> regexResults = new ArrayList<>();
        while (matcher.find()) {
            System.out.println("Full match: " + matcher.group(0));
            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println("Group " + i + ": " + matcher.group(i));
                regexResults.add(matcher.group(i));
            }
        }
        return regexResults;
    }

    public WebElement tryHuntElementInMyWindow(By by, int tries) throws InterruptedException {
        WebElement retorno = null;
        while (tries > 0) {
            try {
                System.out.println("Começando a caça do Elemento :" + by);
                setFound(false);
                elementHuntRoadMap(by);
                retorno = getDriver().findElement(by);
                break;
            } catch (Exception e) {
                Thread.sleep(1000);
                tries--;
            }
        }
        return retorno;

    }

    public void waitFullLoad(int timeout) throws InterruptedException {
        for (int timer = 0; timer < timeout; timer++) {
            if (jse.executeScript("return document.readyState").equals("complete")) {
                System.out.println("Pagina carregada.");
                break;
            } else {
                Thread.sleep(999);
            }

        }
    }

    public boolean thereIsIframes() {
        List<WebElement> frames = getDriver().findElements(By.tagName("iframe"));
        if (frames.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * O método waitFor
     *
     * @param by
     * @param timeOut
     * @return
     * @throws InterruptedException
     */
    public WebElement waitFor(By by, int timeOut) throws InterruptedException {
        System.out.println("Aguardando por " + by.toString());
        while (timeOut > 0) {
            try {
                return this.driver.findElement(by);
            } catch (ElementNotFoundException | NoSuchElementException e) {
                System.out.println("\ttentativa " + String.valueOf(timeOut));
                Thread.sleep(1000);
                timeOut--;
            }
        }
        throw new NotFoundException("Elemento " + by.toString() + " Não Encontrado");
    }

    // MÉTODO PARA FACILITAR MUDANÇAS DE FRAME
    public void switchFrame(int frame) throws InterruptedException {
        if (frame == -1) {
            this.driver.switchTo().defaultContent();
            return;
        }
        Thread.sleep(500);

        this.driver.switchTo().defaultContent();
        Thread.sleep(1000);

        List<WebElement> frames;
        frames = this.driver.findElements(By.tagName("iframe"));
        this.driver.switchTo().frame(frames.get(frame));
    }

    public int contaFrames() throws InterruptedException {
        this.driver.switchTo().defaultContent();
        Thread.sleep(1000);
        return this.driver.findElements(By.tagName("iframe")).size();
    }

    public int contaFramesCascata() throws InterruptedException {
        Thread.sleep(1000);
        return this.driver.findElements(By.tagName("iframe")).size();
    }

    //MÉTODO PARA MUDANÇA DE FRAMES DENTRO DE OUTROS FRAMES
    public void switchFrameCascata(int frame) throws InterruptedException {
        Thread.sleep(1000);
        if (frame == -1) {
            this.driver.switchTo().defaultContent();
            return;
        }
        List<WebElement> frames;
        frames = this.driver.findElements(By.tagName("iframe"));
        Thread.sleep(1000);

        this.driver.switchTo().frame(frames.get(frame));
    }

    public List<WebElement> waitForElements(By by, int timeOut) throws InterruptedException {
        System.out.println("Aguardando por " + by.toString());

        while (timeOut > 0) {
            try {
                return this.driver.findElements(by);
            } catch (ElementNotFoundException | NoSuchElementException e) {
                System.out.println("\ttentativa " + String.valueOf(timeOut));
                Thread.sleep(1000);
                timeOut--;
            }
        }
        throw new NotFoundException("Elemento " + by.toString() + " Não Encontrado");
    }

    /**
     * O método recebe por parametro o título de uma Janela do Navegador
     * instanciado que o driver deverá assumir.
     *
     * @param title
     */
    public void switchToWindowByTitle(String title) {
        for (String janela : driver.getWindowHandles()) {
            driver.switchTo().window(janela);
            if (driver.getTitle().contains(title)) {
                break;
            }
            System.out.println("STATUS: PROCURANDO JANELA DE " + title.toUpperCase());
        }
    }

    /**
     * O método Switch to Simple Frame recebe um elemento By que corresponde ao
     * By do frame para qual o driver deverá assumir.
     *
     * @param by
     */
    public void switchToSimpleFrame(By by) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(by));
    }

    /**
     * O método Switch to Simple Frame recebe uma String que corresponde ao Nome
     * do frame para qual o driver deverá assumir.
     *
     * @param frameName
     */
    public void switchToSimpleFrame(String frameName) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(frameName);
    }

    private void elementHuntRoadMap(By by, String map) {
        if (!found) {
            navigatoToMapLocation(map);
            try {
                driver.findElement(by);
                System.out.println("Elemento Encontrado em: " + map);
                setFound(true);
            } catch (Exception e) {
                List<WebElement> options = getDriver().findElements(By.tagName("iframe"));
                if (options.size() > 0) {
                    System.out.println("Estou em: " + map + "\nEncontrei:");
                } else {
                    System.out.println("Nenhum Frame encontrado em: " + map + "Retornando...");
                }
                for (WebElement option : options) {

                    System.out.println(option.getAttribute("name"));
                }
                for (WebElement option : options) {
                    navigatoToMapLocation(map);
                    elementHuntRoadMap(by, map + "/" + option.getAttribute("name"));
                }
            }
        }
    }

    private void navigatoToMapLocation(String map) {
        String[] dirs = map.split("/");
        driver.switchTo().defaultContent();
        for (String dir : dirs) {
            if (dir.substring(0, 1).equals("*")) {
                System.out.println(dir.substring(1, dir.length()));
                getDriver().switchTo().window(dir.substring(1, dir.length()));
            } else {
                getDriver().switchTo().frame(dir);
            }
        }
    }

    public boolean elementExist(By by) {
        boolean elementExist;
        try {
            driver.findElement(by);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void elementHuntRoadMap(By by) {
        if (!found) {
            getDriver().switchTo().defaultContent();
            try {
                driver.findElement(by);
                System.out.println("Elemento Encontrado na raiz");
                setFound(true);
            } catch (Exception e) {
                List<WebElement> options = getDriver().findElements(By.tagName("iframe"));
                System.out.println("Frames iniciais:");
                for (WebElement option : options) {
                    System.out.println("\t" + option.getAttribute("Name"));
                }
                for (WebElement option : options) {
                    if (!found) {
                        try {
                            getDriver().switchTo().defaultContent();
                            System.out.println("Entrando no frame: " + option.getAttribute("name"));
                            elementHuntRoadMap(by, option.getAttribute("name"));
                        } catch (Exception ee) {
                            System.out.println(ee);
                        }

                    } else {
                        return;
                    }

                }
            }
        } else {
            return;
        }
    }

    /**
     * @return the found
     */
    public boolean isFound() {
        return found;
    }

    /**
     * @param found the found to set
     */
    public void setFound(boolean found) {
        this.found = found;
    }

    /**
     * @return the waitLoad
     */
    public boolean isWaitLoad() {
        return waitLoad;
    }

    /**
     * @param waitLoad the waitLoad to set
     */
    public void setWaitLoad(String waitLoad) {
        if (waitLoad == "fast") {
            this.getDriver().manage().timeouts().implicitlyWait(750, TimeUnit.MILLISECONDS);
        } else if (waitLoad == "slow") {
            this.getDriver().manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        } else if (waitLoad == "medium") {
            this.getDriver().manage().timeouts().implicitlyWait(1800, TimeUnit.MILLISECONDS);
        } else if (waitLoad == "zero") {
            this.getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * @return the driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * @return the executorJs
     */
    /**
     * @return the jse
     */
    public JavascriptExecutor getJse() {
        return jse;
    }

    /**
     * @param jse the jse to set
     */
    public void setJse(JavascriptExecutor jse) {
        this.jse = jse;
    }

    /**
     * @return the action
     */
    public Actions getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(Actions action) {
        this.action = action;
    }
}
