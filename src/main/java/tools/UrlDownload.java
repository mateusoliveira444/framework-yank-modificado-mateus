/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import utils.SystemUtils;

/**
 *
 * @author CodeMaster
 */
public class UrlDownload {

    public static String Download(String url,String filePath, String nomeArquivo) { 
        String path = "";
        if(SystemUtils.getOperationalSystem().toUpperCase().contains("WINDOWS")){
            path = filePath +"\\"+ nomeArquivo;
        }else{
            path = filePath +"/"+ nomeArquivo;
        }
        
        try {
            FileUtils.copyURLToFile(new URL(url), new File(path), 10000, 10000);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(UrlDownload.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return path;

    }


}