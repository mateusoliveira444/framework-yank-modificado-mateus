package dao;

import bean.AcessoYank;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A Classe Acesso Yank Dao possui métodos para capturar informações dos Robôs Yank! cadastrados no Painel
 * @author tmont
 */
public class AcessoYankDao {
//    Para Conexões que não exigem proxy use esse método
    private OkHttpClient client = DaoFactory.WSConnection();
    
//    para conexões que Exigem Proxy use esse metódo passando numero do Proxy do cliente e porta.
//    private OkHttpClient client = DaoFactory.WSproxyConnection("nro.do.proxy.cliente", 3336);
    
//    Insira o id do Robo no lugar de 0 (localizado no Painél após o cadastro do Robo)
    private int idRobo = 0;
    private String sistema;
    
    public AcessoYankDao(int idRobo, String sistema) {
        this.idRobo = idRobo;
        this.sistema = sistema;
    }
    
    /**
     * O método Dados Acesso Captura os dados de Acesso do Robo e os retorna em um objeto AcessoYank(bean)
     * @return 
     */
    public AcessoYank dadosAcesso() {
        AcessoYank acessoYank = new AcessoYank();
        try {
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "SELECT robo_usuario, robo_senha, robo_sistema FROM robo_x_usuario WHERE robo_id = " + idRobo + " AND robo_sistema like '" + sistema + "%'");
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = getClient().newCall(request).execute();
            String json = response.body().string();
            JSONArray jsonList = null;
            try{
                jsonList = new JSONArray(json);
            }catch(Exception e){
                JSONObject jErro = new JSONObject(json);
                System.out.println("Erro");
                System.out.println(jErro.get("mensagem").toString());
                System.in.read();
            }
            
            for(int i = 0; i < jsonList.length(); i++){
		JSONObject jObject = jsonList.getJSONObject(i);
                acessoYank.setUsuario(jObject.get("robo_usuario").toString());
                acessoYank.setSenha(jObject.get("robo_senha").toString());
                String urls[] = jObject.get("robo_sistema").toString().split(";");
                for (int j = 1; j < urls.length; j++) {
                    if(StringUtils.isBlank(acessoYank.getUrl())){
                        acessoYank.setUrl(urls[j]);
                    }else{
                        acessoYank.setUrl(acessoYank.getUrl()+";"+urls[j]);
                    }
                }
                
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }

        return acessoYank;
    }
    
    
    /**
     * @return the client
     */
    public OkHttpClient getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(OkHttpClient client) {
        this.client = client;
    }
    

}