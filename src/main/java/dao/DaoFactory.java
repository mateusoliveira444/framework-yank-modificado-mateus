/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DaoFactory {

    private static OkHttpClient client;
    private static Calendar dataAtual;
    private String tabela = "StringTabela";

    /**
     * O método WSConnection é Estático e pode ser Chamado a qualquer momento,
     * de qualquer lugar. Ele retorna um cliente de conexão com o Web service
     * Yank!
     *
     * @return
     */
    public DaoFactory() {
        client = WSConnection();
        dataAtual = new GregorianCalendar();
    }

    public static OkHttpClient WSConnection() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = new OkHttpClient();
        client = builder.build();
        return client;
    }

    public static String dataHoraAtual() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dataAtual = new GregorianCalendar();
        Date now = dataAtual.getTime();
        return sdf.format(now);
    }

    public void realizaUpdate(Object generic) throws JSONException {
        JSONObject jo = new JSONObject();
        jo.put("status_execucao", generic.getClass());
        jo.put("instancia", "1");
        jo.put("status", generic.getClass());

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jo.toString());
        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/update/" + tabela + "/" + "generic.getId()")
                .post(body)
                .addHeader("username", "yank")
                .addHeader("password", "Y@nk123")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        String json = null;
        while (true) {
            try {
                response = client.newCall(request).execute();
                json = response.body().string();
                break;
            } catch (Exception e) {
                System.out.println("STATUS: ERRO AO REALIZAR UPDATE, VERIFICAR");
                e.printStackTrace();
            }
        }

        response.close();

        try {
            JSONObject jObjRetornoWS = new JSONObject(json);
            System.out.println(jObjRetornoWS.get("mensagem").toString());
        } catch (Exception e) {
            System.out.println("ERRO AO CAPTURAR RETORNO DO WS AO REALIZAR UPDATE, VERIFICAR");
            e.printStackTrace();
        }

    }

    public List<Object> realizaSelect() {
        List<Object> listaObjetos = new ArrayList<>();
        Object varObjeto = null;
        try {

            MediaType mediaType = MediaType.parse("text/plain");

            String query = "SELECT EMAIL, CPF, id FROM " + tabela + " WHERE status = '0'";
            RequestBody body = RequestBody.create(mediaType, query);
            Request request = new Request.Builder()
                    .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/select")
                    .post(body)
                    .addHeader("username", "yank")
                    .addHeader("password", "Y@nk123")
                    .addHeader("content-type", "text/plain")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = null;

            response = client.newCall(request).execute();
            String json = response.body().string();
            System.out.println(json);
            response.close();

            JSONArray jsonList = null;
            try {
                jsonList = new JSONArray(json);
            } catch (Exception e) {
                JSONObject jErro = new JSONObject(json);
                System.out.println("Erro");
                System.out.println(jErro.get("mensagem").toString());
            }

            for (int i = 0; i < jsonList.length(); i++) {
                JSONObject jObject = jsonList.getJSONObject(i);
                Object objeto = new Object();
//                objeto.setCampo((jObject.get(Campo).toString()));
//                objeto.setId(jObject.getInt("id"));

//                Objetos.add(objeto);
            }
            

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro ao capturar informações do caso");
        }
        return listaObjetos;
    }

    public void realizaInsert(Object objeto) throws JSONException {

        JSONObject jo = null;

        jo = new JSONObject();

        JSONArray ja = new JSONArray();

//        jo.put("STATUS", objeto.getStatus());
        jo.put("TIME_EXEC", "NOW()");
        jo.put("TIME_IMPORT", dataHoraAtual());
        jo.put("STATUS_EXECUCAO", "ARQUIVO NÃO ENCONTRADO");
        jo.put("INSTANCIA", 1);
        ja.put(jo);

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, ja.toString());
        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/inserts/" + tabela)
                .post(body)
                .addHeader("username", "yank")
                .addHeader("password", "Y@nk123")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        String json = null;
        while (true) {
            try {
                response = client.newCall(request).execute();
                json = response.body().string();
                break;
            } catch (Exception e) {
                System.out.println("ERRO AO CONECTAR COM BD YANK: " + e);
                e.printStackTrace();
            }
        }

        response.close();

        try {
            JSONObject jObjRetornoWS = new JSONObject(json);
            jObjRetornoWS.get("mensagem").toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERRO AO CAPTURAR RETORNO DO WS AO INSERT");
        }

        System.out.println("STATUS: DADOS INSERIDOS COM SUCESSO!");
    }

}
