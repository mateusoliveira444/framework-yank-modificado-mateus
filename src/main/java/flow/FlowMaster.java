/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

//import com.gargoylesoftware.htmlunit.javascript.host.event.InputEvent;
import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import java.io.IOException;
import java.awt.event.InputEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Sleeper;
import org.sikuli.script.FindFailed;
import utils.PDFUtils;
import utils.SeleniumUtils;
import utils.SikuliUtils;
import utils.SystemUtils;

/**
 *
 * @author Max
 */
public class FlowMaster {

    public static Scanner scanner = new Scanner(System.in);

    public FlowMaster() {

    }

    /**
     * Após efetuar todos os procedimentos necessários antes do fluxo do Robo..
     * Iniamos o Fluxo no Método Start da Classe FlowMaster É aqui que vai todo
     * o Código do Fluxo do Robo.
     *
     * @throws IOException
     * @throws AWTException
     * @throws InterruptedException
     * @throws FindFailed
     */
    public void start() throws IOException, AWTException, InterruptedException, FindFailed, FileNotFoundException, InvalidFormatException {
        System.out.println("Iniciando Fluxo");
        //Para Instanciar o Sikuli, Basta dar um Nome da instancia e indicar onde estarão as imagens.
        SikuliUtils sk = new SikuliUtils("\\img\\");
        sk.click("teste.png");
        //Para Instanciar um navegador Selenium basta dar um nome da instancia
        SeleniumUtils chrome = new SeleniumUtils();
        //depois Chame a função que constrói o Navegador Desejado,
        chrome.buildChromeDriver();
        /*uma vez construído o navegado estara setado na variável Driver... 
        Utilize o método getDriver() para manipulá-lo*/
        chrome.getDriver().get("http://www.google.com");
        /*caso precise utilizar códigos em JavaScript... 
        Junto ao driver do navegador é instanciado um JsExecutor na variável jse
        Utilize o método getJse() para manipulá-lo*/
        chrome.getJse().executeScript("alert('Este é um código em JavaScript!');");
        //Divirta-se...   
        

       
    }
}
