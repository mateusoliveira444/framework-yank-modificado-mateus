/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import utils.SeleniumUtils;
import utils.SystemUtils;
import static utils.SystemUtils.deleteFile;
import static utils.SystemUtils.scanner;
import static utils.SystemUtils.setRegistryLifeSaver;
import tools.UrlDownload;

/**
 *
 * @author Max
 */
public class EnviromentSetUp {

    private String operationalSystem;
    private String arch;
    private List<String> browsers = null;

    public EnviromentSetUp() {

    }

    public void start() throws IOException, InterruptedException {
        getEnviromentInfo();

    }

    private void getEnviromentInfo() throws IOException, InterruptedException {
        System.out.println("Iniciando Captura de informações do Ambiente...");
        System.out.println("Identificando Sistema Operacional...");
        setOperationalSystem(SystemUtils.getOperationalSystem());
        System.out.println(getOperationalSystem() + "\n");
        System.out.println("Identificando Arquitetura...");
        setArch(SystemUtils.getSystemArch());
        System.out.println(getArch() + "\n");
        if (getOperationalSystem().toUpperCase().contains("WINDOWS")) {
            setWindows();
        } else if (getOperationalSystem().toUpperCase().contains("LINUX")) {
            setLinux();
        }
        System.out.println("Dados do Sistema Capturados.");
    }

    public static void setWindows() throws IOException, InterruptedException {
        boolean chrome = false;
        boolean firefox = false;
        boolean ie = false;
        /*Configura o projeto para Trabalhar em Ambiente Windows.*/
        System.out.println("Quais navegadores o Robo irá utilizar? Digite as opções separadas por ';'");
        List<String> Browsers = new ArrayList<String>();
        System.out.println("[1]Internet Explorer [2]Chrome [3]Firefox");
        String[] options = scanner.next().toString().split(";");
        for (String option : options) {
            if (option.contains("1")) {
                Browsers.add("Internet Explorer");
                ie = true;
            } else if (option.contains("2")) {
                Browsers.add("Chrome");
                chrome = true;
            } else if (option.contains("3")) {
                Browsers.add("Firefox");
                firefox = true;
            }
        }
        System.out.println("Navegadores:");
        for (String navegador : Browsers) {
            System.out.println("\t" + navegador);
        }
        System.out.println("\n");
        System.out.println("Deseja Instalar/Atualizar os Navegadores? 'S' ou 'N'");
        if (scanner.next().toUpperCase().contains("S")) {
            System.out.println("\tAtualizando/Instalando Navegadores");
            for (String browser : Browsers) {
                if (browser.equals("Internet Explorer")) {
                    System.out.println("Configurando Internet Explorer");
                    setRegistryLifeSaver();
                    ie = true;
                } else if (browser.equals("Chrome")) {
                    chrome = true;
                } else if (browser.equals("Firefox")) {
                    firefox = true;
                }
            }
            if (chrome == true && firefox == true) {
                System.out.println("Configurando Chrome e Firefox");
                UrlDownload.Download("https://ninite.com/chrome-firefox/ninite.exe", System.getProperty("user.dir"), "browsers.exe");

            } else if (chrome == true) {
                System.out.println("configurando Chrome");
                UrlDownload.Download("https://ninite.com/chrome/ninite.exe", System.getProperty("user.dir"), "browsers.exe");
            } else if (firefox == true) {
                System.out.println("configurando Firefox");
                UrlDownload.Download("https://ninite.com/firefox/ninite.exe", System.getProperty("user.dir"), "browsers.exe");
            }
            if (chrome == true || firefox == true) {
                try {
                    Process p = Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\browsers.exe");
                    while (p.isAlive()) {
                        System.out.println("Atualizando Navegadores... Feche o Instalador quando terminar");
                        Thread.sleep(10000);
                    }
                    System.out.println("Programa terminou normalmente");
                    System.out.println("Deletando Arquivo...");
                    SystemUtils.deleteFile(System.getProperty("user.dir") + "\\browsers.exe");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Dispensando a Instalação/Atualização de Navegadores");
        }
        System.out.println("Deseja Instalar os WebDrivers? 'S' ou 'N'");
        if (scanner.next().toUpperCase().contains("S")) {
            if (ie == true) {
                byte[] buffer = new byte[1024];
                UrlDownload.Download("http://selenium-release.storage.googleapis.com/3.9/IEDriverServer_Win32_3.9.0.zip", System.getProperty("user.dir") + "\\drivers\\", "IEDriver.zip");
                ZipInputStream zinstream = new ZipInputStream(new FileInputStream(System.getProperty("user.dir") + "\\drivers\\IEDriver.zip"));
                ZipEntry zentry = zinstream.getNextEntry();
                while (zentry != null) {
                    String entryName = zentry.getName();
                    FileOutputStream outstream = new FileOutputStream(System.getProperty("user.dir") + "\\drivers\\" + entryName);
                    int n;
                    while ((n = zinstream.read(buffer)) > -1) {
                        outstream.write(buffer, 0, n);
                    }
                    outstream.close();
                    // Fecha entrada e tenta pegar a proxima
                    zinstream.closeEntry();
                    zentry = zinstream.getNextEntry();
                }
                zinstream.close();
                SystemUtils.deleteFile(System.getProperty("user.dir") + "\\drivers\\IEDriver.zip");
            }
            if (chrome == true) {
                byte[] buffer = new byte[1024];
                UrlDownload.Download("https://chromedriver.storage.googleapis.com/2.38/chromedriver_win32.zip", System.getProperty("user.dir") + "\\drivers\\", "chrome.zip");
                ZipInputStream zinstream = new ZipInputStream(new FileInputStream(System.getProperty("user.dir") + "\\drivers\\chrome.zip"));
                ZipEntry zentry = zinstream.getNextEntry();
                while (zentry != null) {
                    String entryName = zentry.getName();
                    FileOutputStream outstream = new FileOutputStream(System.getProperty("user.dir") + "\\drivers\\" + entryName);
                    int n;
                    while ((n = zinstream.read(buffer)) > -1) {
                        outstream.write(buffer, 0, n);
                    }
                    outstream.close();
                    // Fecha entrada e tenta pegar a proxima
                    zinstream.closeEntry();
                    zentry = zinstream.getNextEntry();
                }
                zinstream.close();
                SystemUtils.deleteFile(System.getProperty("user.dir") + "\\drivers\\chrome.zip");
            }

        }
        if (firefox == true) {
            String url = "";
            if (SystemUtils.getSystemArch().contains("64")) {
                url = "https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-win64.zip";
            } else {
                url = "https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-win32.zip";
            }
            byte[] buffer = new byte[1024];
            UrlDownload.Download(url, System.getProperty("user.dir") + "\\drivers\\", "firefox.zip");
            ZipInputStream zinstream = new ZipInputStream(new FileInputStream(System.getProperty("user.dir") + "\\drivers\\firefox.zip"));
            ZipEntry zentry = zinstream.getNextEntry();
            while (zentry != null) {
                String entryName = zentry.getName();
                FileOutputStream outstream = new FileOutputStream(System.getProperty("user.dir") + "\\drivers\\" + entryName);
                int n;
                while ((n = zinstream.read(buffer)) > -1) {
                    outstream.write(buffer, 0, n);
                }
                outstream.close();
                // Fecha entrada e tenta pegar a proxima
                zinstream.closeEntry();
                zentry = zinstream.getNextEntry();
            }
            zinstream.close();
            SystemUtils.deleteFile(System.getProperty("user.dir") + "\\drivers\\firefox.zip");
        }
        System.out.println("A Configuração de Ambiente terminou....");
        Thread.sleep(5000);
    }

    /**
     * @return the operationalSystem
     */
    public String getOperationalSystem() {
        return operationalSystem;
    }

    /**
     * @param operationalSystem the operationalSystem to set
     */
    public void setOperationalSystem(String operationalSystem) {
        this.operationalSystem = operationalSystem;
    }

    /**
     * @return the arch
     */
    public String getArch() {
        return arch;
    }

    /**
     * @param arch the arch to set
     */
    public void setArch(String arch) {
        this.arch = arch;
    }

    /**
     * @return the browsers
     */
    public List<String> getBrowsers() {
        return browsers;
    }

    /**
     * @param browsers the browsers to set
     */
    public void setBrowsers(List<String> browsers) {
        this.browsers = browsers;
    }

    private void setLinux() throws FileNotFoundException, IOException {
        String pkgManager = "";
        System.out.println("Tentando identificar Gerenciador de Pacotes");
        if (SystemUtils.executeCommand("apt -v").toUpperCase().contains("COMMAND")) {
            if (SystemUtils.executeCommand("dnf -v").toUpperCase().contains("COMMAND")) {
                System.out.println("Não foi possível Configurar o Ambiente.");
                return;
            } else {
                System.out.println("Gerenciador de Pacotes: DNF");
                pkgManager = "dnf";
            }
        } else {
            System.out.println("Gerenciador de pacotes: Synaptics");
            pkgManager = "apt";

        }
        System.out.println(System.getProperty("user.dir"));
        System.out.println(SystemUtils.executeCommand("mkdir files drivers"));
        System.out.println("Quais navegadores o Robo irá utilizar? Digite as opções separadas por ';'");
        List<String> Browsers = new ArrayList<String>();
        System.out.println("[2]Chrome [3]Firefox");
        String[] options = scanner.next().toString().split(";");
        for (String option : options) {
            if (option.contains("2")) {
                System.out.println("Instalando/atualizando Chrome Driver");
                UrlDownload.Download("https://chromedriver.storage.googleapis.com/2.38/chromedriver_linux64.zip", System.getProperty("user.dir") + "/drivers/", "chrome.zip");
                byte[] buffer = new byte[1024];
                ZipInputStream zinstream = new ZipInputStream(new FileInputStream(System.getProperty("user.dir") + "/drivers/chrome.zip"));
                ZipEntry zentry = zinstream.getNextEntry();
                while (zentry != null) {
                    String entryName = zentry.getName();
                    FileOutputStream outstream = new FileOutputStream(System.getProperty("user.dir") + "/drivers/" + entryName);
                    int n;
                    while ((n = zinstream.read(buffer)) > -1) {
                        outstream.write(buffer, 0, n);
                    }
                    outstream.close();
                    // Fecha entrada e tenta pegar a proxima
                    zinstream.closeEntry();
                    zentry = zinstream.getNextEntry();
                }
                zinstream.close();
                System.out.println(SystemUtils.executeCommand("chmod +x " + System.getProperty("user.dir") + "/drivers/chromedriver"));
                SystemUtils.deleteFile(System.getProperty("user.dir") + "/drivers/chrome.zip");
            } else if (option.contains("3")) {
                System.out.println("Instalando/atualizando Firefox Driver");
                String url = "";
                if (SystemUtils.getSystemArch().contains("64")) {
                    url = "https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-linux64.tar.gz";
                } else {
                    url = "https://github.com/mozilla/geckodriver/releases/download/v0.20.1/geckodriver-v0.20.1-linux32.tar.gz";
                }
                UrlDownload.Download(url, System.getProperty("user.dir") + "/drivers/", "firefox.tar.gz");
                SystemUtils.executeCommand("tar -vzxf " + System.getProperty("user.dir") + "/drivers/firefox.tar.gz");
                SystemUtils.deleteFile(System.getProperty("user.dir") + "/drivers/firefox.tar.gz");
                SystemUtils.executeCommand("mv geckodriver " + System.getProperty("user.dir") + "/drivers/");
                System.out.println(SystemUtils.executeCommand("chmod +x " + System.getProperty("user.dir") + "/drivers/geckodriver"));
                
            }
        }

    }

}
