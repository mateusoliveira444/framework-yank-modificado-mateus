package main;

import flow.EnviromentSetUp;
import flow.FlowMaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.sikuli.script.FindFailed;
import utils.SystemUtils;

/**
 *
 * @author max
 */
public class Main {

    /**
     * Esta é a classe principal do projeto, onde o código começará a ser executado.
     * Aqui faremos as conexões com o WS para adquirir as credenciais do Robo,
     *  e qualquer outro procedimento necessário antes do início do fluxo.
     * @param args
     * @throws InterruptedException
     * @throws IOException
     * @throws FindFailed
     * @throws Exception 
     */
    public static void main(String[] args) throws InterruptedException, IOException, FindFailed, Exception {
        /*Através da classe EnviromentSetUp é possível configurar o ambiente de trabalho do Robô
        independe do sistema operacional ou Configurações de Hardware.
        Tambem é possível baixar e Configurar os navegadores desejados, 
        assim como seus Respectívos drivers para manipulação no Selenium.
        Apenas deixe-o descomentado caso não precise de configurar o Ambiente.
         */
//        EnviromentSetUp setup = new EnviromentSetUp();
//        setup.start();

        /*O Flow Master é onde você irá programar o robô para seguir o Fluxo desejado pelo cliente*/
        FlowMaster flow = new FlowMaster();
        flow.start();
    }
}
