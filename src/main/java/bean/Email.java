/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.Date;

/**
 *
 * @author Mateus Oliveira
 */
public class Email {
    private String data;
    private String hora;
    private String numeroPasta;
    private String idEmail;
    private String timeImport;
    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the numeroPasta
     */
    public String getNumeroPasta() {
        return numeroPasta;
    }

    /**
     * @param numeroPasta the numeroPasta to set
     */
    public void setNumeroPasta(String numeroPasta) {
        this.numeroPasta = numeroPasta;
    }

    /**
     * @return the idEmail
     */
    public String getIdEmail() {
        return idEmail;
    }

    /**
     * @param idEmail the idEmail to set
     */
    public void setIdEmail(String idEmail) {
        this.idEmail = idEmail;
    }

    /**
     * @return the timeImport
     */
    public String getTimeImport() {
        return timeImport;
    }

    /**
     * @param timeImport the timeImport to set
     */
    public void setTimeImport(String timeImport) {
        this.timeImport = timeImport;
    }

    
}
